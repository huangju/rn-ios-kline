//
//  PushNative.m
//  kChart
//
//  Created by hj on 2018/11/30.
//  Copyright © 2018 Facebook. All rights reserved.
//

//PushNative.m
#import "PushNative.h"
// 导入AppDelegate，获取UINavigationController
#import "AppDelegate.h"
// 导入跳转的页面
#import "TestViewController.h"

@implementation PushNative

//导出模块
RCT_EXPORT_MODULE();
// RN跳转原生界面
// rnToNative导出方法
RCT_EXPORT_METHOD(rnToNative) {
  //主要这里必须使用主线程发送,不然有可能失效
  dispatch_async(dispatch_get_main_queue(), ^{
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UINavigationController *rootNav = app.navController;
    
    TestViewController *test = [[TestViewController alloc] init];
    
    [rootNav pushViewController:test animated:YES];
  });
}

@end
