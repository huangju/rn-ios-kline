//
//  PushNative.h
//  kChart
//
//  Created by hj on 2018/11/30.
//  Copyright © 2018 Facebook. All rights reserved.
//

//PushNative.h
#import <Foundation/Foundation.h>
// 导入RCTBridgeModule类，这个是react-native提供
#import <React/RCTBridgeModule.h>
// 遵守RCTBridgeModul协议
@interface PushNative : NSObject<RCTBridgeModule>
@end
