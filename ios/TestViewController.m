//
//  TestViewController.m
//  kChart
//
//  Created by hj on 2018/11/30.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "TestViewController.h"
#import "YKLineChart.h"

@implementation TestViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  self.view.backgroundColor = [UIColor whiteColor];
  self.title = @"这是页面";
  
  YKLineChartView *kChart = [[YKLineChartView alloc] init];
  
  kChart = [[YKLineChartView alloc] initWithFrame:CGRectMake(50, 100, 300, 200)];
  
  NSString * path =[[NSBundle mainBundle]pathForResource:@"data.plist" ofType:nil];
  NSArray * sourceArray = [[NSDictionary dictionaryWithContentsOfFile:path] objectForKey:@"data"];
  NSMutableArray * array = [NSMutableArray array];

  for (NSDictionary * dic in sourceArray) {
    
    YKLineEntity * entity = [[YKLineEntity alloc]init];
    entity.high = [dic[@"high_px"] doubleValue];
    entity.open = [dic[@"open_px"] doubleValue];
    
    entity.low = [dic[@"low_px"] doubleValue];
    
    entity.close = [dic[@"close_px"] doubleValue];
    
    entity.date = dic[@"date"];
    entity.ma5 = [dic[@"avg5"] doubleValue];
    entity.ma10 = [dic[@"avg10"] doubleValue];
    entity.ma20 = [dic[@"avg20"] doubleValue];
    entity.volume = [dic[@"total_volume_trade"] doubleValue];
    [array addObject:entity];
    //YTimeLineEntity * entity = [[YTimeLineEntity alloc]init];
  }
  
  [array addObjectsFromArray:array];

  // 设置图表
  YKLineDataSet * dataset = [[YKLineDataSet alloc]init];
  dataset.data = array;
  dataset.highlightLineColor = [UIColor colorWithRed:60/255.0 green:76/255.0 blue:109/255.0 alpha:1.0];
  dataset.highlightLineWidth = 0.7;
  dataset.candleRiseColor = [UIColor colorWithRed:233/255.0 green:47/255.0 blue:68/255.0 alpha:1.0];
  dataset.candleFallColor = [UIColor colorWithRed:33/255.0 green:179/255.0 blue:77/255.0 alpha:1.0];
  dataset.avgLineWidth = 1.f;
  dataset.avgMA10Color = [UIColor colorWithRed:252/255.0 green:85/255.0 blue:198/255.0 alpha:1.0];
  dataset.avgMA5Color = [UIColor colorWithRed:67/255.0 green:85/255.0 blue:109/255.0 alpha:1.0];
  dataset.avgMA20Color = [UIColor colorWithRed:216/255.0 green:192/255.0 blue:44/255.0 alpha:1.0];
  dataset.candleTopBottmLineWidth = 1;
  
  [kChart setupChartOffsetWithLeft:50 top:10 right:10 bottom:10];
  kChart.gridBackgroundColor = [UIColor whiteColor];
  kChart.borderColor = [UIColor colorWithRed:203/255.0 green:215/255.0 blue:224/255.0 alpha:1.0];
  kChart.borderWidth = .5;
  kChart.candleWidth = 8;
  kChart.candleMaxWidth = 30;
  kChart.candleMinWidth = 1;
  kChart.uperChartHeightScale = 0.7;
  kChart.xAxisHeitht = 25;
  kChart.highlightLineShowEnabled = YES;
  kChart.zoomEnabled = YES;
  kChart.scrollEnabled = YES;
  //给图表添加数据
  [kChart setupData:dataset];
  [self.view addSubview:kChart];
    
}


@end
